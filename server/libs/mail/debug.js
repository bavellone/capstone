var config = require('../../config');

module.exports = function (Message) {

	return function DebugMessage(ops) {
		ops = ops || {};
		_.defaults(ops, {
			to: config.mail.debug.email,
			subject: 'Debug - CapstoneViz',
			from_email: 'capstone@bavellone.me',
			from_name: 'CapstoneViz'
		});

		return new Message(ops);
	}
};
