/*eslint-env node*/
import express from 'express';
import fse from 'fs-extra';
import path from 'path';
import request from 'request';

import config from '../../config';


let gridData = [];

fse.readJson(path.resolve('./data/grid.json'), (err, data) => {
	if (err)
		return next(err);

	gridData = data;
});

module.exports = function GridModule() {
	// Attach the CRUD endpoint
	let app = express.Router();

	app.get('/', getJSONData);
	app.get('/realtime', getRealtimeJSONData);

	return app;
};

function getJSONData(req, res, next) {
	res.json(gridData)
}

function getRealtimeJSONData(req, res, next) {
	request(`http://${config.gridInterface}:${config.gridPort}`, (err, response) => {
		if (err) {
			console.error(err);
			return next(new Error(err));
		}

		console.log('Got response');
		console.log(response.body.length);

		res.json(JSON.parse(response.body));
	});
}
