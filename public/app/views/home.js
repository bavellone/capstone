/*eslint-env browser*/
'use strict';

import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import d3 from 'd3';

import API from '../lib/api';


export default class Home extends React.Component {
	static route = 'home';
	static linkText = 'Home';
	static linkIcon = 'home';

	static defaultProps = {
		menuAnimationDelay: 500,
		width: window.innerWidth,
		height: window.innerHeight,
		updateInterval: 1250,
		numLines: 20,
		numLineChecks: 4,
		vehicleWidth: 10,
		robotOrigin: {
			x: window.innerWidth / 2,
			y: window.innerHeight
		},
		realtime: true
	};

	constructor(props) {
		super(props);
		this.updateLines();
	}

	state = {
		gridData: [],
		frame: 0,
		data: [],
		fetchRealtime: true,
		numLines: 20
	};

	componentDidMount() {
		if (!this.props.realtime)
			API.grid.get().then(data => {
				this.setState({
					gridData: data,
					data: data[0].data
				}, () => {
					this.cycleFrames();
				});
			});
		else
			this.getLatestGridData();
	}
	
	updateLines = () => {
		console.log(this.frameData.height);
		this.lines = this.genLines(this.state.numLines, {
			x: this.props.robotOrigin.x,
			y: this.props.robotOrigin.y * (this.frameData.height / this.frameData.width)
		}, 1000);
	};

	getLatestGridData = () => {
		console.log('fetching data');
		API.grid.realtime().then(data => {
			// console.log(data);
			this.setState({
				gridData: [data],
				data: data.data,
				err: null,
				numLines: data.pathCount
			}, () => {
				this.updateLines();
				if (this.state.fetchRealtime)
					setTimeout(::this.getLatestGridData, this.props.updateInterval);
			});
		}, err => {
			console.log('Got error');
			setTimeout(this.getLatestGridData, this.props.updateInterval);
			this.setState({
				err
			});
			
		})
	};

	componentDidUpdate() {
		const numRows = this.frameData.height;
		const numCols = this.frameData.width;
		
		const gridRatio = numRows / numCols;
		const screenRatio = this.props.width / this.props.height;
		
		console.log(gridRatio);
		
		// console.log(numRows, numCols, this.props.height, this.props.width);
		//
		// console.log(`height ${this.props.height / numRows} width ${this.props.width / numCols}`);
		//
		// console.log(this.state.data.length);

		const grid = d3.select('#grid-viz')
			.attr('width', this.props.width)
			.attr('height', this.props.height * gridRatio);
		
		grid.selectAll('g.grid-row').remove();

		const gridRow = grid.selectAll('g.grid-row')
			.data(this.state.data);
		
		gridRow
			.enter().append('g')
			.attr('class', 'grid-row')
			.attr('index', (d,i) => i)
			.attr('transform', (d, i) => {
				return `translate(0, ${Math.round(i * this.props.height * gridRatio / numRows)})`;
			});
		
		gridRow.exit().remove();

		const gridSquare = gridRow
			.selectAll('rect.grid-square')
			.data(d => d);

		gridSquare
			.enter()
			.append('rect');

		gridSquare
			.attr('class', (d, i) => {
				return d > 0 ? 'grid-square hasObject' : 'grid-square'
			})
			.attr('index', (d,i) => i)
			.attr('width', d => this.props.width / numCols)
			.attr('height', d => Math.ceil(this.props.height * gridRatio / numRows))
			.attr('transform', (d, i) => {
				return `translate(${i * this.props.width / numCols}, 0)`;
			});

		const line = d3.svg.line()
			.x(d => d.x)
			.y(d => d.y);

		// this.updatePaths();
		
		grid.selectAll('path').remove();
		
		grid.selectAll('path')
			.data(this.lines)
			.enter()
			.append('path')
			.attr('class', (d, i) => classnames('line', {
				selected: i == this.state.gridData[0].bestPath
			}))
			.attr('d', line);
	}

	genLines = (numLines, origin, length) =>
		_.map(_.range(numLines), i =>
			this.genLineData(origin.x, origin.y, -(i * Math.PI/numLines), length)
		);

	genLineData = (x, y, angle, length) => {
		return [
			{x, y, angle, length},
			{x: x + Math.cos(angle) * length, y: y + Math.sin(angle) * length, angle, length}
		]
	};

	cycleFrames = () => {
		this.cycling = setInterval(() => {
			const nextFrame = (this.state.frame + 1) % this.state.gridData.length;
			this.setState({
				frame: nextFrame,
				data: this.state.gridData[nextFrame].data
			})
		}, this.props.updateInterval)
	};

	updatePaths = () =>
		_.map(this.lines, (line, i) => {
			const dx = Math.cos(i * line.angle);
			const dy = Math.sin(i * line.angle);
			const dw = this.props.vehicleWidth/2;

			let min = line.length;

			// Check additional parallel lines around the current line
			_.map(_.range(-dw, dw, this.props.vehicleWidth / this.state.numLines), m => {
				let len = 0;

				// Check the length of all lines to find the shortest
				_.map(_.range(Math.floor(line.length - m * Math.cos(line.angle))), j => {
					const cellY = Math.floor(this.props.robotOrigin.y - j * dy);
					const cellX = Math.floor(this.props.robotOrigin.x + m + j * dx);

					// if (this.state.data[cellY][cellX])
				})
			});
		});

	toggleRealtime = () => {
		if (!this.state.fetchRealtime)
			this.getLatestGridData();

		this.setState({
			fetchRealtime: !this.state.fetchRealtime
		});

	};

	get frameData() {
		return this.state.gridData[this.state.frame] || {width:100, height:100};
	}

	render() {
		return (
			<div id="app-body-container">

				<div id="app-body">
					<div className="view home">
						<div className="ui view home inverted vertical masthead center aligned segment">

							<div className="ui container">
								<div className="ui large secondary inverted pointing menu">
									<a className="toc item">
										<i className="sidebar icon"/>
									</a>

								</div>
							</div>

							<div className="ui text container">
								<h1 className="ui inverted header">
									CapstoneViz
								</h1>
								<h2 className="ui inverted header">
									Visualizer Toolbox
									</h2>

									<button className='ui primary button' onClick={this.toggleRealtime}>{this.state.fetchRealtime ? 'Stop realtime streaming' : 'Start realtime streaming'}</button>

							</div>

						</div>
						
						<div className={classnames("ui warning message", {hidden: !this.state.err})}>
							<div className="header">Error</div>
							{this.state.err}
						</div>



						<svg id="grid-viz"></svg>

					</div>

				</div>
			</div>
		);
	}
}
